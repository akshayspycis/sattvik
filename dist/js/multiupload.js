function multiUploader(config){
	this.config = config;
	this.items = "";
	this.all = {}
	var self = this;
	multiUploader.prototype._init = function(){
		if (window.File && 
			window.FileReader && 
			window.FileList && 
			window.Blob) {		
			 var inputId = $("#"+this.config.form).find("input[type='file']").eq(0).attr("id");
			 document.getElementById(inputId).addEventListener("change", this._read, false);
			 document.getElementById(this.config.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
			 document.getElementById(this.config.dragArea).addEventListener("drop", this._dropFiles, false);
			 document.getElementById(this.config.form).addEventListener("submit", this._submit, false);
		} else
			console.log("Browser supports failed");
	}
	
	multiUploader.prototype._submit = function(e){
		e.stopPropagation(); e.preventDefault();
		self._startUpload();
	}
        
        
	multiUploader.prototype._preview = function(data){
            		this.items = data;
		if(this.items.length > 0 && this.items.length <10){
			var html;		
			var uId = "";
 			for(var i = 0; i<this.items.length; i++){
				uId = this.items[i].name._unique();
                                obj={};
                                obj["file"]=this.items[i];
                                self.all[uId]=obj;
                                var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){
                                    e.stopPropagation();
                                    self._deleteFiles($(this).parent().parent().attr('rel'));
                                    $(this).parent().parent().remove();
                                });
				var sampleIcon = 'fa fa-image';
				var errorClass = "";
				if(typeof this.items[i] != undefined){
                                    if(self._validate(this.items[i].type) <= 0) {
                                            sampleIcon = 'fa fa-exclamation';
                                            errorClass =" invalid";
                                    } 
                                    html=$('<div></div>').addClass('dfiles'+errorClass).attr({'rel':uId,'id':'as'+uId})
                                        .append($('<h5></h5>').append('<i class="'+sampleIcon+'"></i>&nbsp&nbsp'+this.items[i].name+'"</i>')
                                        .append('<img src="../../dist/images/loading_1.gif" style="position:absolute;right:20px;top:2px;height:20px;width:20px;"/>')
                                        .append(kk))
                                        .click(function (e){
                                            e.stopPropagation();
                                    });
				}
                                $("#dragAndDropFiles").append(html);
                                readfiles(this.items[i],uId)
		}
	}else{
            alert("Image file select limit maximum is 10 files.")
        }
    }

        multiUploader.prototype.setStream = function(stream,sid){
		self.all[sid]["byte_stream"]=stream;
                $(".dfiles[rel='"+sid+"'] >h5>img").remove();
	}
        
	multiUploader.prototype._read = function(evt){
		if(evt.target.files){
                    self._preview(evt.target.files);
                            //self._preview(evt.target.files);
		} else 
			console.log("Failed file reading");
	}
	
	multiUploader.prototype._validate = function(format){
		var arr = this.config.support.split(",");
		return arr.indexOf(format);
	}
	
	multiUploader.prototype._dropFiles = function(e){
		e.stopPropagation(); 
                e.preventDefault();
                self._preview(e.dataTransfer.files);
	}
        multiUploader.prototype._deleteFiles = function(key){
            delete self.all[key];
	}
	multiUploader.prototype._uploader = function(file,key){
		if(typeof file != undefined && self._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="../../dist/images/fb_ty.gif" style="position:absolute;right:20px;top:2px;height:20px;width:20px;"/>')
			$.ajax({
				type:"POST",
                                url:"/mocktest_v1//Img",
				data:{'images':file["byte_stream"]},
//				cache: false,
//				contentType: false,
//				processData: false,
				success:function(rponse){
                                    $(".dfiles[rel='"+ids+"']").remove();
                                        self._deleteFiles(key);
				}
			});
		}else
                    console.log("Invalid file format - "+file.name);
	}
	multiUploader.prototype._startUpload = function(){
                    $.each(self.all,function(key,value){
                        self._uploader(value,key);
                    });
	}
	String.prototype._unique = function(){
		return this.replace(/[a-zA-Z]/g, function(c){
                    return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                });
	}

	this._init();
}
var a;
function initMultiUploader(){
	a=new multiUploader(config);
}

$('#dragAndDropFiles').click(function(e) {
    $(this).find('input[type="file"]').click();
});

$('#demoFiler input').click(function(e) {
    e.stopPropagation();
});
